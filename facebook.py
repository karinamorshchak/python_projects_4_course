import time

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select


def check_exists_by_xpath(xpath):
    try:
        driver.find_element(By.XPATH, xpath)
    except NoSuchElementException:
        return False
    return True


ser = Service("c:\selenium browser drivers\chromedriver.exe")
driver = webdriver.Chrome(service=ser)
driver.maximize_window()
driver.get("http://www.fb.com")
print(driver.current_url)
if driver.current_url == "https://www.facebook.com/":
    if check_exists_by_xpath("//*[text()='Создать новый аккаунт']"):
        driver.find_element(By.XPATH, "//*[text()='Создать новый аккаунт']").click()
        time.sleep(2)
        driver.find_element(By.NAME, "firstname").send_keys("Marina")
        driver.find_element(By.NAME, "lastname").send_keys("Kravchenko")
        driver.find_element(By.NAME, "reg_email__").send_keys("some_email_for_lab3@gmail.com")
        driver.find_element(By.NAME, "reg_email_confirmation__").send_keys("some_email_for_lab3@gmail.com")
        driver.find_element(By.ID, "password_step_input").send_keys("passw123")
        day = Select(driver.find_element(By.XPATH, "//select[@title='День']"))
        day.select_by_visible_text("5")
        month = Select(driver.find_element(By.NAME, "birthday_month"))
        month.select_by_visible_text("мар")
        year = Select(driver.find_element(By.NAME, "birthday_year"))
        year.select_by_visible_text("1999")
        driver.find_element(By.XPATH, "//label[text()='Женщина']").click()
        driver.find_element(By.XPATH, "//button[text()='Регистрация']").click()
    else:
        print("Element is not find")

else:
    print("Wrong url")
